# docker-node

A docker `alpine` container focusing on node but also provides `docker`, `docker-compose` and `nvm`. If you need non alpine based images please utilize the main [node docker fork](https://hub.docker.com/_/node).

This fork is intended to make things easier when your use case requires docker and possible docker-compose.

## Semver

see: [npm version](https://docs.npmjs.com/cli/version)
