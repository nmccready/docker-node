ARG DOCKER_VERSION

FROM docker:$DOCKER_VERSION

ARG NODE_VERSION
ARG YARN_VERSION=1.22.4
# https://stackoverflow.com/questions/52196518/could-not-get-uid-gid-when-building-node-docker
ARG NPM_UNSAFE
#start add custom scripts
ENV YARN_VERSION $YARN_VERSION

RUN mkdir -p /root/.docker

RUN \
  apk update && \
  apk add --no-cache coreutils bash bash-doc bash-completion zip curl wget jq git \
  g++ gcc gnupg make \
  python docker-compose binutils-gold g++ libstdc++ \
  gcc gnupg libgcc linux-headers make python shadow && \
  touch /root/.bashrc && \
  echo source /etc/profile >> /root/.bashrc

ENV ENV="/root/.bashrc"
SHELL ["bash", "-c"]

# default all users to bash
RUN sed -i 's/\/bin\/ash/\/bin\/bash/g' /etc/passwd

RUN mkdir -p /root/.nvm
ENV NVM_DIR=/root/.nvm
# NOTE: unofficial ... is used by node official for alpine releases!
# https://github.com/nodejs/docker-node/blob/c6bc44e84afcdb81d9749b7b034c60e916a519ad/10/alpine3.9/Dockerfile#L21
ENV NVM_NODEJS_ORG_MIRROR=https://unofficial-builds.nodejs.org/download/release

RUN echo ${NODE_VERSION} >> /.nvmrc

# https://github.com/nvm-sh/nvm/issues/1102#issuecomment-550572252
# Force NVM to get the correct architecture and work w/ alpine
# For intial install
RUN echo "nvm_get_arch() { nvm_echo \"x64-musl\"; }" >> /etc/profile
RUN curl https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash

# https://github.com/nvm-sh/nvm/issues/1102#issuecomment-550572252
# Force NVM to get the correct architecture and work w/ alpine
# For all nvm.sh sourcing from other shells
RUN echo "nvm_get_arch() { nvm_echo \"x64-musl\"; }" >> /root/.nvm/nvm.sh

# Install NODE
RUN source $NVM_DIR/nvm.sh && \
    nvm install $NODE_VERSION && \
    if [[ ! -z "$NPM_UNSAFE" ]]; then npm config set unsafe-perm true; fi && \
    nvm use ${NODE_VERSION} && \
    npm i -g yarn@${YARN_VERSION} && \
    yarn config set strict-ssl false && \
    NODE_PATH=$(echo `which node` | sed 's/\/node$//g') && \
    echo "export NODE_PATH=$(echo `which node` | sed 's/\/node$//g')" >> /etc/profile  && \
    echo "export PATH=\$NODE_PATH:\$PATH" >> /etc/profile && \
    ln -s $NODE_PATH/* /usr/local/bin/.
# NOTE: `ln -s $NODE_PATH/* /usr/local/bin/.` is vital to getting k8's /.gitlab-ci working as
# NOTE:   it does not respect CMD, ENTRYPOINTS, and skips profile, and bash login (NOTHING is sources)
# NOTE:   WHAT does this mean... It means it's impossible to naturally load / source nvm.sh
# NOTE:      therefore since we're root only we symbolic link all node binaries to /usr/local/bin
# NOTE:        then nothing needs to be sourced.
#
# ROOT problem: https://gitlab.com/gitlab-org/gitlab-runner/issues/2338#note_98845123
# NOTE: Methods tried:
#  - custom entrypoints which source bashrc which  in turn source nvm.sh
#  - CMD custom bash proxy which also sources
#  - forcing environment variables into /etc/environment (fails seems to be ignored and not sourced) requires explicit source /etc/environment


RUN echo "[ -s \"$NVM_DIR/nvm.sh\" ] && \\. \"$NVM_DIR/nvm.sh\"" >> /etc/profile && \
    echo 'nvm use' >> /etc/profile
